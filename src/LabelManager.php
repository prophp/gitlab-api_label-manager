<?php

namespace ProPhp\GitlabApi\LabelManager;

use ProPhp\GitlabApi\ApiClient;

class LabelManager
{
    private ApiClient $gitlabApi;

    private array $colorsGreenToRed = [
        '00b140', '00b140', '00b140', 'c39953', 'c39953', 'c39953', 'cd5b45', 'cd5b45', 'cd5b45'
        //'00ff00', '40ff00', '80ff00', 'bfff00', 'ffff00', 'ffbf00', 'ff8000', 'ff4000', 'ff0000',
    ];

    private array $difficulty = ['low', 'medium', 'high'];

    private array $importance = ['low', 'medium', 'high'];

    private array $steps = ['*', '**', '***'];

    private array $labelsToGenerate = ['importance', 'difficulty'];

    private array $importancePriorities = [];
    private array $difficultyPriorities = [];


    public function __construct(string $apiToken)
    {
        $this->gitlabApi = new ApiClient($apiToken);
    }

    private function generateLabels(): array
    {
        $result = [];

        foreach ($this->labelsToGenerate as $labelToGenerate) {
            $i = 0;
            foreach ($this->$labelToGenerate as $level) {
                foreach ($this->steps as $step) {
                    $result[] = [
                        'name' => ucfirst($labelToGenerate) . ": $level $step",
                        'color' => "#" . $this->colorsGreenToRed[$i]
                    ];
                    $i++;
                }
            }
        }

        return $result;
    }

    public function createDefaultLabels(string $projectPath): array
    {
        return $this->gitlabApi->createLabels($projectPath, $this->generateLabels());
    }

    public function setPriorityLabels(): array
    {
        $i = 1;
        foreach (array_reverse($this->importance) as $index => $level) {

            foreach (array_reverse($this->steps) as $step) {
                $this->importancePriorities["Importance: $level $step"] = $i++ * 10;
            }
        }

        $i = 1;
        foreach ($this->difficulty as $level) {
            foreach ($this->steps as $step) {
                $this->difficultyPriorities["Difficulty: $level $step"] = $i++;
            }
        }

        foreach ($this->gitlabApi->listAllIssuesWithLabels() as $task) {
            $this->rewritePriorityScoreLabels($task);
        }

        return ['success'];
    }

    private function rewritePriorityScoreLabels(array $issue): void
    {
        $priorityScore = $this->calculateTaskPriorityScore($issue['labels']);
        $labelPriorityScoreIsValid = false;

        $priorityScoreLabels = [];

        foreach ($issue['labels'] as $label) {
            if (str_starts_with($label, '_ps')) {
                $priorityScoreLabels[] = $label;
            }
        }


        if (count($priorityScoreLabels) > 1) {
            throw new \Exception("Issue {$issue['id']} {$issue['title']} has more than 1 priority score labels");
        }

        if (count($priorityScoreLabels) !== 0) {
            $priorityScoreLabel = $priorityScoreLabels[0];

            $labelPriorityScore = substr($priorityScoreLabel, 3);

            if ((int)$labelPriorityScore === (int)$priorityScore) {
                $labelPriorityScoreIsValid = true;
            }

            if (!$labelPriorityScoreIsValid) {
                $response = $this->gitlabApi->deleteLabel($issue['project_id'], $label);
                if ($response !== ['success']) {
                        echo "Cannot remove label '$label' from project '{$issue['project_id']}': " .
                        json_encode($response) . PHP_EOL;
                }
            } else {
                return;
            }
        }

        if ($priorityScore === 110) {
            return;
        }

        // @todo Check that this label is not already existing (No active but just already exists)
        /*$response = */$this->gitlabApi->createLabels($issue['project_id'], [
            ['name' => "_ps$priorityScore", 'color' => "#f2f2f2", 'priority' => $priorityScore]
        ]);
        // @todo Avoid response "Label already exists"

        $this->gitlabApi->addLabelsToIssue($issue['project_id'], $issue['iid'],"_ps$priorityScore");
    }

    /**
     * @param array $taskLabels
     * @return float|int
     * @throws \Exception
     *
     * MIN = 11 (highest priority), MAX = 110 (lowest priority)
     */
    private function calculateTaskPriorityScore(array $taskLabels = [])
    {
        $difficultyLabels = [];
        $importanceLabels = [];

        foreach ($taskLabels as $taskLabel) {
            if (str_starts_with($taskLabel, "Difficulty")) {
                $difficultyLabels[] = $taskLabel;
                continue;
            }
            if (str_starts_with($taskLabel, "Importance")) {
                $importanceLabels[] = $taskLabel;
            }
        }

        if (count($difficultyLabels) > 1) {
            throw new \Exception("Too many Difficulty labels");
        }

        if (count($importanceLabels) > 1) {
            throw new \Exception("Too many Importance labels");
        }


        $difficultyPriority = empty($difficultyLabels) ? 10 : $this->difficultyPriorities[$difficultyLabels[0]] ?? 10;
        $importancePriority = empty($importanceLabels) ? 100 : $this->importancePriorities[$importanceLabels[0]] ?? 100;

        return $difficultyPriority + $importancePriority;
    }
}