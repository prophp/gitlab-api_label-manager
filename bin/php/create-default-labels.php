<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\GitlabApi\LabelManager\LabelManager;

if (empty($argv[2])) {
    throw new Exception("Project path missing (\$argv[2])");
}

$projectPath = $argv[2];

echo json_encode(
    (new LabelManager($apiToken))->createDefaultLabels($projectPath), JSON_PRETTY_PRINT
);
