<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\GitlabApi\LabelManager\LabelManager;

echo json_encode(
    (new LabelManager($apiToken))->setPriorityLabels(), JSON_PRETTY_PRINT
);
