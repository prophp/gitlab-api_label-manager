<?php

$port = $argv[1] ?? 9999;

file_put_contents(
    dirname(__DIR__, 2) . "/docker-compose.yml",
    str_replace(
        '%port%',
        $port,
        file_get_contents(dirname(__DIR__) . "/templates/docker-compose.yml.txt")
    )
);

# Start docker image
passthru("docker-compose up -d");

# Check that all requirements are present
$output = [];
exec("docker-compose exec server bash -c 'rsync --version'", $output);
$rsyncVersion = $output[0];

$output = [];
exec("docker-compose exec server bash -c 'php -v'", $output);
$phpVersion = $output[0];

echo "$rsyncVersion\n$phpVersion\nhttp://localhost:$port/\n";

if (file_exists(dirname(__DIR__, 2) . "/.gitignore")) {

    $existingGitignoreContents = file_get_contents(dirname(__DIR__, 2) . "/.gitignore");

    if(strpos($existingGitignoreContents, "docker-compose.yml") === false){
        file_put_contents(
            dirname(__DIR__, 2) . "/.gitignore",
            $existingGitignoreContents . PHP_EOL . "docker-compose.yml"
        );
    }

    exit;
}

file_put_contents(dirname(__DIR__, 2) . "/.gitignore", "docker-compose.yml");

